package com.example.hp.gra;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.AbsoluteLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class GameEasy extends Fragment{
    View view;
    private TextView licznik;
    private TextView odpowiedz;
    private Button[] buttons;
    private Boolean zly_ruch=false;
    private static final Integer[] cel=new Integer[] {0,1,2,3,4,5,6,7,8};

    private ArrayList<Integer> pola=new ArrayList<Integer>();

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        view=inflater.inflate(R.layout.gameeasy,container,false);
        buttons=findButtons();
        for(int i=0;i<9;i++)
        {
            this.pola.add(i);
        }
        Collections.shuffle(this.pola);

        fillGrid();

        licznik=(TextView) view.findViewById(R.id.licznik);
        odpowiedz=(TextView) view.findViewById(R.id.odpowiedz);

        for(int i=0;i<9;i++){
            buttons[i].setOnClickListener(new View.OnClickListener(){
               public void onClick(View view){
                   move((Button) view);
               }
            });
        }

        licznik.setText("0");
        odpowiedz.setText(R.string.game_feedback_text);
        return view;
    }

    public void move(final Button b){
        zly_ruch=true;
        int butText,butPos,pos;
        butText=Integer.parseInt((String) b.getText());
        butPos=findPos(butText);
        pos=findPos(0);
        switch (pos){
            case 0:
                if(butPos==1 || butPos==3)
                    zly_ruch=false;
                break;
            case 1:
                if(butPos==0 || butPos==2 || butPos==4)
                    zly_ruch=false;
                break;
            case 2:
                if(butPos==1 || butPos==5)
                    zly_ruch=false;
                break;
            case 3:
                if(butPos==0 || butPos==4 || butPos==6)
                    zly_ruch=false;
                break;
            case 4:
                if(butPos==1 || butPos==3 || butPos==5 || butPos==7)
                    zly_ruch=false;
                break;
            case 5:
                if(butPos==2 || butPos==4 || butPos==8)
                    zly_ruch=false;
                break;
            case 6:
                if(butPos==3 || butPos==7)
                    zly_ruch=false;
                break;
            case 7:
                if(butPos==4 || butPos==6 || butPos==8)
                    zly_ruch=false;
                break;
            case 8:
                if(butPos==5 || butPos==7)
                    zly_ruch=false;
                break;
        }
        if(zly_ruch==true){
            odpowiedz.setText("Ruch nie jest możliwy");
            return;
        }
        odpowiedz.setText("Ruch prawidłowy");
        pola.remove(butPos);
        pola.add(butPos,0);
        pola.remove(pos);
        pola.add(pos,butText);

        fillGrid();
        licznik.setText(Integer.toString(Integer.parseInt((String) licznik.getText())+1));

        for(int i=0;i<9;i++){
            if(pola.get(i)!=cel[i])
            {
                return;
            }
        }
        odpowiedz.setText("WYGRAŁEŚ");
    }

    public Button[] findButtons(){
        Button[] but=new Button[9];

        but[0]=(Button) view.findViewById(R.id.ButtonEasy0);
        but[1]=(Button) view.findViewById(R.id.ButtonEasy1);
        but[2]=(Button) view.findViewById(R.id.ButtonEasy2);
        but[3]=(Button) view.findViewById(R.id.ButtonEasy3);
        but[4]=(Button) view.findViewById(R.id.ButtonEasy4);
        but[5]=(Button) view.findViewById(R.id.ButtonEasy5);
        but[6]=(Button) view.findViewById(R.id.ButtonEasy6);
        but[7]=(Button) view.findViewById(R.id.ButtonEasy7);
        but[8]=(Button) view.findViewById(R.id.ButtonEasy8);
        return but;
    }

    public void fillGrid(){
        for(int i=0;i<9;i++){
            int tmp=pola.get(i);
            AbsoluteLayout.LayoutParams paramsAb=((AbsoluteLayout.LayoutParams)buttons[tmp].getLayoutParams());
            switch (i){
                case 0:
                    paramsAb.x=5;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 1:
                    paramsAb.x=185;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 2:
                    paramsAb.x=365;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 3:
                    paramsAb.x=5;
                    paramsAb.y=185;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 4:
                    paramsAb.x=185;
                    paramsAb.y=185;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 5:
                    paramsAb.x=365;
                    paramsAb.y=185;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 6:
                    paramsAb.x=5;
                    paramsAb.y=365;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 7:
                    paramsAb.x=185;
                    paramsAb.y=365;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 8:
                    paramsAb.x=365;
                    paramsAb.y=365;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
            }
        }
    }

    public int findPos(int elem){
        int i=0;
        for(i=0;i<9;i++){
            if(pola.get(i)==elem){
                break;
            }
        }
        return i;
    }
}
