package com.example.hp.gra;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.AbsoluteLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class GameMed extends Fragment{
    View view;
    private TextView licznik;
    private TextView odpowiedz;
    private Button[] buttons;
    private Boolean zly_ruch=false;
    private static final Integer[] cel=new Integer[] {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    private ArrayList<Integer> pola=new ArrayList<Integer>();

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        view=inflater.inflate(R.layout.gamemedium,container,false);
        buttons=findButtons();
        for(int i=0;i<16;i++)
        {
            this.pola.add(i);
        }
        Collections.shuffle(this.pola);

        fillGrid();

        licznik=(TextView) view.findViewById(R.id.licznik);
        odpowiedz=(TextView) view.findViewById(R.id.odpowiedz);

        for(int i=0;i<16;i++){
            buttons[i].setOnClickListener(new View.OnClickListener(){
                public void onClick(View view){
                    move((Button) view);
                }
            });
        }

        licznik.setText("0");
        odpowiedz.setText(R.string.game_feedback_text);
        return view;
    }

    public void move(final Button b){
        zly_ruch=true;
        int butText,butPos,pos;
        butText=Integer.parseInt((String) b.getText());
        butPos=findPos(butText);
        pos=findPos(0);
        switch (pos){
            case 0:
                if(butPos==1 || butPos==4)
                    zly_ruch=false;
                break;
            case 1:
                if(butPos==0 || butPos==2 || butPos==5)
                    zly_ruch=false;
                break;
            case 2:
                if(butPos==1 || butPos==3 || butPos==6)
                    zly_ruch=false;
                break;
            case 3:
                if(butPos==2 || butPos==7)
                    zly_ruch=false;
                break;
            case 4:
                if(butPos==0 || butPos==8 || butPos==5)
                    zly_ruch=false;
                break;
            case 5:
                if(butPos==1 || butPos==4 || butPos==6 || butPos==9)
                    zly_ruch=false;
                break;
            case 6:
                if(butPos==2 || butPos==5 || butPos==7 || butPos==10)
                    zly_ruch=false;
                break;
            case 7:
                if(butPos==3 || butPos==6 || butPos==11)
                    zly_ruch=false;
                break;
            case 8:
                if(butPos==4 || butPos==9 || butPos==12)
                    zly_ruch=false;
                break;
            case 9:
                if(butPos==5 || butPos==8 || butPos==10 || butPos==13)
                    zly_ruch=false;
                break;
            case 10:
                if(butPos==6 || butPos==9 || butPos==11 || butPos==14)
                    zly_ruch=false;
                break;
            case 11:
                if(butPos==7 || butPos==10 || butPos==15)
                    zly_ruch=false;
                break;
            case 12:
                if(butPos==8 || butPos==13)
                    zly_ruch=false;
                break;
            case 13:
                if(butPos==9 || butPos==12 || butPos==14)
                    zly_ruch=false;
                break;
            case 14:
                if(butPos==13 || butPos==10 || butPos==15)
                    zly_ruch=false;
                break;
            case 15:
                if(butPos==11 || butPos==14)
                    zly_ruch=false;
                break;
        }
        if(zly_ruch==true){
            odpowiedz.setText("Ruch nie jest możliwy");
            return;
        }
        odpowiedz.setText("Ruch prawidłowy");
        pola.remove(butPos);
        pola.add(butPos,0);
        pola.remove(pos);
        pola.add(pos,butText);

        fillGrid();
        licznik.setText(Integer.toString(Integer.parseInt((String) licznik.getText())+1));

        for(int i=0;i<16;i++){
            if(pola.get(i)!=cel[i])
            {
                return;
            }
        }
        odpowiedz.setText("WYGRAŁEŚ");
    }

    public Button[] findButtons(){
        Button[] but=new Button[16];

        but[0]=(Button) view.findViewById(R.id.ButtonMed0);
        but[1]=(Button) view.findViewById(R.id.ButtonMed1);
        but[2]=(Button) view.findViewById(R.id.ButtonMed2);
        but[3]=(Button) view.findViewById(R.id.ButtonMed3);
        but[4]=(Button) view.findViewById(R.id.ButtonMed4);
        but[5]=(Button) view.findViewById(R.id.ButtonMed5);
        but[6]=(Button) view.findViewById(R.id.ButtonMed6);
        but[7]=(Button) view.findViewById(R.id.ButtonMed7);
        but[8]=(Button) view.findViewById(R.id.ButtonMed8);
        but[9]=(Button) view.findViewById(R.id.ButtonMed9);
        but[10]=(Button) view.findViewById(R.id.ButtonMed10);
        but[11]=(Button) view.findViewById(R.id.ButtonMed11);
        but[12]=(Button) view.findViewById(R.id.ButtonMed12);
        but[13]=(Button) view.findViewById(R.id.ButtonMed13);
        but[14]=(Button) view.findViewById(R.id.ButtonMed14);
        but[15]=(Button) view.findViewById(R.id.ButtonMed15);
        return but;
    }

    public void fillGrid(){
        for(int i=0;i<16;i++){
            int tmp=pola.get(i);
            AbsoluteLayout.LayoutParams paramsAb=((AbsoluteLayout.LayoutParams)buttons[tmp].getLayoutParams());
            //RelativeLayout.LayoutParams paramsAb=new RelativeLayout.LayoutParams();
            switch (i){
                case 0:
                    paramsAb.x=5;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 1:
                    paramsAb.x=160;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 2:
                    paramsAb.x=315;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 3:
                    paramsAb.x=470;
                    paramsAb.y=5;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 4:
                    paramsAb.x=5;
                    paramsAb.y=160;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 5:
                    paramsAb.x=160;
                    paramsAb.y=160;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 6:
                    paramsAb.x=315;
                    paramsAb.y=160;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 7:
                    paramsAb.x=470;
                    paramsAb.y=160;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 8:
                    paramsAb.x=5;
                    paramsAb.y=315;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 9:
                    paramsAb.x=160;
                    paramsAb.y=315;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 10:
                    paramsAb.x=315;
                    paramsAb.y=315;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 11:
                    paramsAb.x=470;
                    paramsAb.y=315;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 12:
                    paramsAb.x=5;
                    paramsAb.y=470;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 13:
                    paramsAb.x=160;
                    paramsAb.y=470;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 14:
                    paramsAb.x=315;
                    paramsAb.y=470;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
                case 15:
                    paramsAb.x=470;
                    paramsAb.y=470;
                    buttons[tmp].setLayoutParams(paramsAb);
                    break;
            }
        }
    }

    public int findPos(int elem){
        int i=0;
        for(i=0;i<16;i++){
            if(pola.get(i)==elem){
                break;
            }
        }
        return i;
    }
}
